ALTER TABLE critiquer
  ADD PRIMARY KEY (id_r,mail);
ALTER TABLE photo
  ADD PRIMARY KEY (id_p);
ALTER TABLE preferer
  ADD PRIMARY KEY (mail,id_tc);
ALTER TABLE proposer
  ADD PRIMARY KEY (id_r,id_tc);
ALTER TABLE resto
  ADD PRIMARY KEY (id);
ALTER TABLE type_cuisine
  ADD PRIMARY KEY (id_tc);
ALTER TABLE utilisateur
  ADD PRIMARY KEY (mail);

ALTER TABLE critiquer
  ADD CONSTRAINT critiquer_resto_fk FOREIGN KEY (id_r) REFERENCES resto (id),
  ADD CONSTRAINT critiquer_utilisateur_fk FOREIGN KEY (mail) REFERENCES utilisateur (mail);
ALTER TABLE photo
  ADD CONSTRAINT photo_resto_fk FOREIGN KEY (id_r) REFERENCES resto (id);
ALTER TABLE preferer
  ADD CONSTRAINT preferer_utilisateur_fk FOREIGN KEY (mail) REFERENCES utilisateur (mail),
  ADD CONSTRAINT preferer_type_cuisine_fk FOREIGN KEY (id_tc) REFERENCES type_cuisine (id_tc);
ALTER TABLE proposer
  ADD CONSTRAINT proposer_resto_fk FOREIGN KEY (id_r) REFERENCES resto (id),
  ADD CONSTRAINT proposer_type_cuisine_fk FOREIGN KEY (id_tc) REFERENCES type_cuisine (id_tc);

