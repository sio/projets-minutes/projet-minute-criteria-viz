INSERT INTO resto VALUES
(1, 'l''entrepote', '2', 'rue Maurice Ravel', '33000', 'Bordeaux', 44.7948, -0.58754, 'description', 'Ouverture - Midi : 11h45 à 14h30   Soir : de 18h45 à 22h30 - À emporter : de 11h30 à 23h'),
(2, 'le bar du charcutier', '30', 'rue Parlement Sainte-Catherine', '33000', 'Bordeaux', NULL, NULL, 'description', 'Ouverture - Midi : 11h00 à 15h30   Soir : de 17h35 à 23h30 - À emporter : de 11h00 à 00h00'),
(3, 'Sapporo', '33', 'rue Saint Rémi', '33000', 'Bordeaux', NULL, NULL, 'Le Sapporo propose à ses clients de délicieux plats typiques japonais.', 'Ouverture - Midi : 11h45 à 14h30   Soir : de 18h45 à 22h30 - À emporter : de 11h30 à 23h'),
(4, 'Cidrerie du fronton', NULL, 'Place du Fronton', '64210', 'Arbonne', NULL, NULL, 'description', 'Ouverture - Midi : 11h45 à 14h30   Soir : de 18h45 à 22h30 - À emporter : de 11h30 à 23h'),
(5, 'Agadir', '3', 'Rue Sainte-Catherine', '64100', 'Bayonne', NULL, NULL, 'description', 'Ouverture - Midi : 11h45 à 15h30   Soir : de 18h45 à 2h00 - À emporter : de 12h00 à 2h'),
(6, 'Le Bistrot Sainte Cluque', '9', 'Rue Hugues', '64100', 'Bayonne', NULL, NULL, 'description', 'Ouverture - Midi : 11h45 à 14h30   Soir : de 18h45 à 22h30 - À emporter : de 11h30 à 23h'),
(7, 'la petite auberge', '15', 'rue des cordeliers', '64100', 'Bayonne', NULL, NULL, 'description', 'Ouverture - Midi : 11h45 à 14h30   Soir : de 18h45 à 22h30 - À emporter : de 11h30 à 23h'),
(8, 'La table de POTTOKA', '21', 'Quai Amiral Dubourdieu', '64100', 'Bayonne', NULL, NULL, 'description','Ouverture - Midi : 11h15 à 14h45   Soir : de 18h00 à 23h45 - À emporter : de 11h00 à 23h30'),
(9, 'La Rotisserie du Roy Léon', '8', 'rue de coursic', '64100', 'Bayonne', NULL, NULL, 'description', 'Ouverture - Midi : 11h45 à 15h30   Soir : de 19h00 à 21h30 - À emporter : de 11h30 à 22h'),
(10, 'Bar du Marché', '39', 'Rue des Basques', '64100', 'Bayonne', NULL, NULL, 'description', 'Ouverture - Midi : 11h15 à 13h30   Soir : de 18h30 à 21h00 - À emporter : de 11h00 à 21h'),
(11, 'Trinquet Moderne', '60', 'Avenue Dubrocq', '64100', 'Bayonne', NULL, NULL, 'description', 'Ouverture - Midi : 12h15 à 16h30   Soir : de 19h à 2h30 - À emporter : de 11h30 à 23h55');
