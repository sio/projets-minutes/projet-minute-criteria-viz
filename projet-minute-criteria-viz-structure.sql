CREATE TABLE critiquer (
  id_r integer NOT NULL,
  mail varchar(150) NOT NULL,
  note integer DEFAULT NULL,
  commentaire varchar(4096) DEFAULT NULL
);
CREATE TABLE photo (
  id_p integer  NOT NULL,
  chemin varchar(255) DEFAULT NULL,
  id_r integer DEFAULT NULL
);
CREATE TABLE preferer (
  mail varchar(150) NOT NULL,
  id_tc integer NOT NULL
);
CREATE TABLE proposer (
  id_r integer NOT NULL,
  id_tc integer NOT NULL
);
CREATE TABLE resto (
  id integer NOT NULL,
  nom varchar(255) DEFAULT NULL,
  num_adr varchar(20) DEFAULT NULL,
  voie_adr varchar(255) DEFAULT NULL,
  cp char(5) DEFAULT NULL,
  ville varchar(255) DEFAULT NULL,
  latitude float DEFAULT NULL,
  longitude float DEFAULT NULL,
  desc_resto text,
  horaires text
);
CREATE TABLE type_cuisine (
  id_tc integer NOT NULL,
  libelle varchar(255) DEFAULT NULL
);
CREATE TABLE utilisateur (
  mail varchar(150) NOT NULL,
  mdp varchar(50) DEFAULT NULL,
  pseudo varchar(50) DEFAULT NULL
);
